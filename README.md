**Tools**
----
* **Framework**
  `Flask (Python)`
  `flask_restful`
* **DataBase**
  `Atlas`



**Seach Result**
----
* Returns a JSON array of data with matching results. 
* **URL**
  `/med`
* **method:**
  `PUT`
* **Data Params**
  **Required:**
  `id=1`
  `name=[string]`
  `chem=[string]`
  `manu=[string]`
  `license=[string]`
* **Sample Call**    
      `:---`
      ```javascript
             fetch(`https://url/med`, {
                 method: "PUT",
                 body: JSON.stringify({
                     "id" : -1 ,
                     "name" : "ant",
                     "chem": "menthol",
                     "manu" : "",
                     "license" : "",  
                 }),
                 headers: {
                     'Content-type': 'application/json; charset=UTF-8'
                 }
             }).then(res=> res.json())
      ```



**Post Data**
----
* Returns the JSON object if successfully inserted to database. 
* **URL**
  `/med`
* **method:**
  `PUT`
* **Data Params**
  **Required:**
  `name=[string]`
  `searchName=[string]`
  `chemicals=[string]`
  `searchChem=[string]`
  `manufacturer=[string]`
  `searchManu=[string]`
  `type=[string]`
  `price=[float]`
  `license=[string]`
  `image=[string (base64 representation)]`
 
*  **Not Required:**
   `id=[integer]`
   `description=[string]`



# [Api deployed on Heroku](https://med-digu.herokuapp.com/ "Api")

## FrontEnd Demo
![](demo.gif)