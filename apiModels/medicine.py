from flask_restful import Resource,abort
from flask_cors import cross_origin
import flask_mongoengine
from models.medicine import MedModel,db
from parsers.medicine import *
import json

def toSearchText(text):
	pre = text.replace(" ", "")
	return pre.lower()


class apiMed(Resource):
	@cross_origin()
	def put(self):
		args = med_pet_args.parse_args()
		if(args['id'] == -1):
			Q = flask_mongoengine.mongoengine.queryset.visitor.Q 
			result = []
			if(args['license'] != ""):
				result = MedModel.objects(license_=args['license'])
			else :
				result = MedModel.objects(
						Q(searchName__contains=toSearchText(args['name'])) &
						Q(searchChem__contains=toSearchText(args['chem'])) &
						Q(searchManu__contains=toSearchText(args['manu'])) 
					)
			res = list(map(lambda x: x.json(), result))
			return json.dumps(res,indent=4)
		else :
			args = med_put_args.parse_args()
			result = MedModel.objects(license_=args['license'])
			if result:
				abort(409, message="Medicine is already in database")

			med = MedModel( 
					name=args['name'], 
					searchName=args['searchName'], 
					chemicals =  args['chemicals'],
					searchChem = args['searchChem'],
					license_ =  args['license'],
					manufacturer =  args['manufacturer'],
					searchManu = args['searchManu'],
					type_ = args['type'],
					price =  args['price'],
					description = args['description']
				)
			if(args['image'] !=  "") :
				med.image = args['image']
			if(args['searchName'] != ""):
				med.searchName = args['searchName']
			else :
				med.searchName = toSearchText(args['name'])
			if(args['searchChem'] != ""):
				med.searchChem = args['searchChem']
			else :
				med.searchChem = toSearchText(args['chemicals'])
			if(args['searchManu'] != ""):
				med.searchManu = args['searchManu']
			else :
				med.searchManu = toSearchText(args['manufacturer'])
			med.save()
			return med.json(), 201
	@cross_origin()
	def patch(self):
		args = med_update_args.parse_args()
		result = MedModel.objects(license_=args['license'])
		if not result:
			abort(404, message="Medicine doesn't exist, cannot be updated")
		result = result[0]
		if args['name'] != "":
			result.name = args['name']
		if agrs['searchName'] != "" :
 			result.searchName=args['searchName']
		if args['chemicals']  != "": 
			result.chemicals =  args['chemicals']
		if args['searchChem'] != "" : 
			result.searchChem = args['searchChem']
		if args['manufacturer'] != "" :
			result.manufacturer =  args['manufacturer']
		if args['searchManu'] != "" :
			result.searchManu = args['searchManu']
		if args['type'] != "" :
			result.type_ = args['type']
		if args['price'] != -1 :
			result.price =  args['price']
		if args['image'] != "" :
			result.image = args['image']
		if args['description'] != "" :
			result.description = args['description']

		result.save()

		return result.json()


	
