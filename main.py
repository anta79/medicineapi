from flask import Flask
from flask_restful import Api,Resource
from models.medicine import db 
from flask_cors import CORS, cross_origin

class status(Resource) :
	def get(self):
		try :
			return {'data':'Api is running'}
		except :
			return {'data': 'There is some error'}


app = Flask(__name__)
api = Api(app)
app.config['MONGODB_SETTINGS'] = {
    'host': "mongodb+srv://digu:{}@pythoncluster.xtday.mongodb.net/{}?retryWrites=true&w=majority".format("db_pass","db_name")
}
# app.config['MONGODB_SETTINGS'] = {
#     'host': 'mongodb://localhost:27017/digubase'
# }
cors = CORS(app) 
app.config["CORS_HEADERS"] = "Content-Type"
db.init_app(app)

from apiModels.medicine import apiMed 
api.add_resource(status,'/')
api.add_resource(apiMed,"/med")

if __name__ == "__main__":
	app.run(debug=True)